					<?php if($permintaan != NULL) { ?>
						<table class="w3-rest w3-center w3-table w3-bottombar w3-striped w3-theme-l1 w3-theme-light w3-border-theme">
							<thead class="w3-theme color-accent-text">
								<tr style="font-weight: 900">
									<th>No.</th>
									<th>Kecamatan</th>
									<th>Permintaan</th>
									<th>Opsi</th>
								</tr>
							</thead>
							
							<?php
							$i = 1;
							
							foreach($permintaan as $data) { ?>
								<tr>
									<td><?= $i; ?></td>
									<td><?= $data->nama_kecamatan; ?></td>
									<td><span class="permintaan-<?= $i; ?>"><?= $data->permintaan; ?></span><input id="<?= $jenis; ?>-input-<?= $data->id; ?>" class="w3-input w3-small permintaan-edit-<?= $i; ?>" type="text" style="display:none" value="<?= $data->permintaan; ?>"></td>
									<td>
										<div class="permintaan-<?= $i; ?>">
											<span id="<?= $jenis; ?>-view-btn-<?= $data->id; ?>" class="w3-tag w3-round w3-theme-d1 cursor-pointer" onclick="viewClick(<?= $data->id; ?>, <?= $data->lat; ?>, <?= $data->lang; ?>)" href="#"><i class="material-icons color-base-text w3-small">remove_red_eye</i></span>
											<span class="w3-tag w3-round w3-theme-d1 cursor-pointer" onclick="editClick(<?= $i; ?>)" href="#"><i class="material-icons color-base-text w3-small">edit</i></span>
										</div>
										
										<div class="permintaan-edit-<?= $i; ?>" style="display:none">
											<span class="w3-tag w3-round w3-green cursor-pointer" onclick="saveClick('<?= $jenis; ?>', <?= $data->id; ?>)" href="#"><i class="material-icons color-base-text w3-small">done</i></span>
											<span class="w3-tag w3-round w3-red cursor-pointer" onclick="cancelClick(<?= $i; ?>)" href="#"><i class="material-icons color-base-text w3-small">clear</i></span>
										</div>
									</td>
								</tr>
							<?php $i++; } ?>
							
							
						</table>
					<?php } else { ?>
						<div class="w3-rest w3-center w3-theme-l1 w3-border-theme">
							<p>Belum ada data.</p></p>
						</div>
					<?php } ?>

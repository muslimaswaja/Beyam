<!DOCTYPE html>
<html>
	<head>
		<title>Beyam - Pemetaan Pak Bejan Jualan Ayam</title>
		
		<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/w3.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>css/w3-theme.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
		
		<script src="<?= base_url(); ?>js/w3.js"></script>
		
		<style>
			@font-face {
				font-family: opensans;
				src: url(<?= base_url(); ?>src/opensans.ttf);
			}

			@font-face {
				font-family: greatlakes;
				src: url(<?= base_url(); ?>src/GREALN__.ttf);
			}

			.font-great h1{
				font-family: greatlakes;
				color: white;
				font-size: 50px;
				z-index: 100;
				margin-top: 35px;
				padding-bottom: 25px;
			}
			
			body {
				font-family: opensans;
				height: 100%;
				margin: 0;
			}
			
			#map {
				height: 100%;
			}
			
			.color-base {
				background-color: #ecf0f1;
				color: #2c3e50;
			}
			
			.color-base-text {
				color: #ecf0f1;
			}
			
			.color-accent {
				background-color: #2c3e50;
				color: #ecf0f1;
			}
			
			.color-accent-text {
				color: #2c3e50;
			}

			@media screen and (max-width: 600px){
			    ul.topnav li.right, 
			    ul.topnav li {float: none;}
			}

			.w3-theme {
				opacity: 0.8;
			}

			div.background {
				background-attachment: fixed;
			    background-image: url(img/chick.jpg);
			    background-size: cover;
			    background-blend-mode: overlay;
			    background-repeat: no-repeat;
			    background-position: center;
			/*    margin: 0;*/
			    display: block;
			    position: absolute;
			    left: 0;
			    top: 0;
			    height: 150px;
			    z-index: -100;
			    width: 100%;
			}

			.color-accent-border {
				border-color: #2c3e50 !important;
			}
			
			.cursor-pointer {
				cursor: pointer;
			}
		</style>
	</head>
	
	<body class="color-base">
		<!-- Header -->

		<div>
			<div class="background w3-theme">
			</div>
			<div class="font-great">
				<center><h1>BEYAM</h1></center>
			</div>
		</div>

		<!-- End Header -->

		<!-- Menu Tab -->
		
		<div class="menu">
			<nav class="color-accent w3-container w3-card-2">
				<div class="w3-col l5 w3-right">&nbsp;</div>
				<div class="w3-col l5 w3-left">&nbsp;</div>
				
				<a id="dagingBtn" style="font-size:18px; margin-bottom:0; text-decoration:none" class= "w3-bottombar w3-border-theme w3-padding w3-col l1 w3-center" href="#" onclick="openDaging()">DAGING</a>
				<a id="telurBtn" style="font-size:18px; margin-bottom:0; text-decoration:none" class="w3-padding color-accent-border w3-bottombar w3-col l1 w3-center w3-hover-border-theme" href="#" onclick="openTelur()">TELUR</a>
			</nav>
		</div>
		
		<script>
			function openTelur(){
				w3.removeClass('#dagingBtn','w3-border-theme');
				w3.addClass('#dagingBtn','color-accent-border w3-hover-border-theme');
				w3.addClass('#telurBtn','w3-border-theme');
				w3.removeClass('#telurBtn','color-accent-border w3-hover-border-theme');
				
				initMap(12);
				showTabel("telur");
			}
			
			function openDaging(){
				w3.removeClass('#telurBtn','w3-border-theme');
				w3.addClass('#telurBtn','color-accent-border w3-hover-border-theme');
				w3.addClass('#dagingBtn','w3-border-theme');
				w3.removeClass('#dagingBtn','color-accent-border w3-hover-border-theme');
				
				initMap();
				showTabel("daging");
			}
		</script>
		
		<!-- End Menu Tab -->

		<!-- Map -->
		
		<br>
		
		<div class="content">
			<div class="w3-row w3-margin">
				<div class="w3-col l1 w3-left">
					&nbsp;
				</div>
				
				<div class="w3-col l1 w3-right">
					&nbsp;
				</div>
				
				<div class="w3-rest w3-border" style="height:500px">
					<div id="map"></div>
				</div>
			</div>
			
			<script>
				var map;

				function initMap(zoomSize=11,lat=-7.6433138,lang=111.356045) {
					map = new google.maps.Map(document.getElementById('map'), {
						center: {lat: lat, lng: lang},
						zoom: zoomSize
					});
					// ini tambahan baru
					// Polygon Bendo
					var bendoPol = [
						{lat: -7.597785, lng: 111.426996}, {lat: -7.611227, lng: 111.415323},
						{lat: -7.613779, lng: 111.405881}, {lat: -7.628582, lng: 111.405195},
						{lat: -7.636579, lng: 111.391118}, {lat: -7.642193, lng: 111.390775},
						{lat: -7.643895, lng: 111.385282}, {lat: -7.652571, lng: 111.385282},
						{lat: -7.652912, lng: 111.394380}, {lat: -7.662269, lng: 111.398671},
						{lat: -7.675028, lng: 111.431630}, {lat: -7.675028, lng: 111.434205},
						{lat: -7.673837, lng: 111.444677}, {lat: -7.659206, lng: 111.460813},
						{lat: -7.660568, lng: 111.479524}, {lat: -7.635388, lng: 111.465619},
						{lat: -7.624328, lng: 111.454633}, {lat: -7.618884, lng: 111.454805},
						{lat: -7.621776, lng: 111.447080}, {lat: -7.634707, lng: 111.442617},
						{lat: -7.630624, lng: 111.430944}, {lat: -7.611397, lng: 111.424249},
						{lat: -7.598976, lng: 111.428369}
					];
					//contruct the polygon
					var bendoPoly = new google.maps.Polygon({
						paths: bendoPol, strokeColor: '#FF0000',
						strokeOpacity: 0.8, strokeWeight: 3,
						fillColor: '#FF0000', fillOpacity: 0.35
					});
					bendoPoly.setMap(map);

					//add listener
					bendoPoly.addListener('click', showArrays);

					infoWindow = new google.maps.InfoWindow;

					//Polygon Sukomoro
					var sukomoroPol = [
						{lat: -7.588935, lng: 111.402381}, {lat: -7.605270, lng: 111.382640},
						{lat: -7.609353, lng: 111.373027}, {lat: -7.605440, lng: 111.363585},
						{lat: -7.607822, lng: 111.360496}, {lat: -7.605099, lng: 111.359466},
						{lat: -7.599995, lng: 111.364787}, {lat: -7.594210, lng: 111.360667},
						{lat: -7.597613, lng: 111.348136}, {lat: -7.600675, lng: 111.343158},
						{lat: -7.605610, lng: 111.345046}, {lat: -7.611735, lng: 111.342814},
						{lat: -7.611225, lng: 111.339553}, {lat: -7.618882, lng: 111.335776},
						{lat: -7.622455, lng: 111.339381}, {lat: -7.623135, lng: 111.345046},
						{lat: -7.622455, lng: 111.355174}, {lat: -7.620413, lng: 111.362041},
						{lat: -7.625177, lng: 111.365474}, {lat: -7.630621, lng: 111.361354},
						{lat: -7.648397, lng: 111.357403}, {lat: -7.647971, lng: 111.362810},
						{lat: -7.649843, lng: 111.363325}, {lat: -7.650183, lng: 111.360751},
						{lat: -7.662773, lng: 111.355000}, {lat: -7.661242, lng: 111.394825}, 
						{lat: -7.652735, lng: 111.394654}, {lat: -7.652565, lng: 111.384526},
						{lat: -7.643888, lng: 111.385556}, {lat: -7.642357, lng: 111.390705},
						{lat: -7.636232, lng: 111.391564}, {lat: -7.628575, lng: 111.405297}
					];

					//contruct the polygon
					var sukomoroPoly = new google.maps.Polygon({
						paths: sukomoroPol, strokeColor: '#FF0000',
						strokeOpacity: 0.8, strokeWeight: 3,
						fillColor: '#FF0000', fillOpacity: 0.35
					});
					sukomoroPoly.setMap(map);

					//add listener
					sukomoroPoly.addListener('click', showArrays);

					infoWindow = new google.maps.InfoWindow;
					
					// Polygon Ngariboyo
					var ngariboyoPol = [
						{lat: -7.666001, lng: 111.303365}, {lat: -7.668383, lng: 111.299245},
						{lat: -7.672806, lng: 111.297185}, {lat: -7.678250, lng: 111.301305},
						{lat: -7.689138, lng: 111.299931}, {lat: -7.689138, lng: 111.303365},
						{lat: -7.683354, lng: 111.306111}, {lat: -7.688117, lng: 111.307828},
						{lat: -7.694241, lng: 111.298558}, {lat: -7.704108, lng: 111.301991},
						{lat: -7.708871, lng: 111.296841}, {lat: -7.711933, lng: 111.300275},
						{lat: -7.714315, lng: 111.295811}, {lat: -7.721119, lng: 111.299245},
						{lat: -7.707170, lng: 111.339070}, {lat: -7.698324, lng: 111.342160},
						{lat: -7.703087, lng: 111.366193}, {lat: -7.700366, lng: 111.374089},
						{lat: -7.694582, lng: 111.375806}, {lat: -7.693221, lng: 111.370999},
						{lat: -7.687437, lng: 111.368253}, {lat: -7.681653, lng: 111.383702},
						{lat: -7.676889, lng: 111.384389}, {lat: -7.677230, lng: 111.387479},
						{lat: -7.673487, lng: 111.388509}, {lat: -7.666001, lng: 111.372716}
					];

					//contruct the polygon
					var ngariboyoPoly = new google.maps.Polygon({
						paths: ngariboyoPol, strokeColor: '#FF0000',
						strokeOpacity: 0.8, strokeWeight: 3,
						fillColor: '#FF0000', fillOpacity: 0.35
					});
					ngariboyoPoly.setMap(map);

					//add listener
					ngariboyoPoly.addListener('click', showArrays);

					infoWindow = new google.maps.InfoWindow;

					//Kawedanan Kawedanan
					var kawedananPol = [
						{lat: -7.673753, lng: 111.443966}, {lat: -7.671882, lng: 111.435383},
						{lat: -7.672052, lng: 111.432293}, {lat: -7.673072, lng: 111.430920},
						{lat: -7.666608, lng: 111.423195}, {lat: -7.665587, lng: 111.413239},
						{lat: -7.662865, lng: 111.408432}, {lat: -7.665076, lng: 111.406201},
						{lat: -7.662525, lng: 111.404828}, {lat: -7.662695, lng: 111.372899},
						{lat: -7.666438, lng: 111.372384}, {lat: -7.670861, lng: 111.380280},
						{lat: -7.675284, lng: 111.389550}, {lat: -7.678346, lng: 111.388176},
						{lat: -7.677155, lng: 111.385086}, {lat: -7.682769, lng: 111.383713},
						{lat: -7.687873, lng: 111.368607}, {lat: -7.693487, lng: 111.371182},
						{lat: -7.695018, lng: 111.376503}, {lat: -7.700291, lng: 111.373070},
						{lat: -7.709648, lng: 111.381653}, {lat: -7.711689, lng: 111.385086},
						{lat: -7.722916, lng: 111.395729}, {lat: -7.729720, lng: 111.433495},
						{lat: -7.720534, lng: 111.435555}, {lat: -7.712199, lng: 111.435727}, 
						{lat: -7.701482, lng: 111.427315}, {lat: -7.691275, lng: 111.430405},
						{lat: -7.691786, lng: 111.436585}, {lat: -7.690425, lng: 111.437272},
						{lat: -7.685321, lng: 111.435212}, {lat: -7.685321, lng: 111.440533},
						{lat: -7.683280, lng: 111.441220}, {lat: -7.682769, lng: 111.443108}
					];

					//contruct the polygon
					var kawedananPoly = new google.maps.Polygon({
						paths: kawedananPol, strokeColor: '#FF0000',
						strokeOpacity: 0.8, strokeWeight: 3,
						fillColor: '#FF0000', fillOpacity: 0.35
					});
					kawedananPoly.setMap(map);

					//add listener
					kawedananPoly.addListener('click', showArrays);

					infoWindow = new google.maps.InfoWindow;

					//Kawedanan Magetan
					var magetanPol = [
						{lat: -7.638904, lng: 111.355915}, {lat: -7.640690, lng: 111.353941},
						{lat: -7.638479, lng: 111.353340}, {lat: -7.638904, lng: 111.351624},
						{lat: -7.638138, lng: 111.351195}, {lat: -7.638223, lng: 111.350165},
						{lat: -7.638989, lng: 111.350250}, {lat: -7.639244, lng: 111.348362},
						{lat: -7.637968, lng: 111.347075}, {lat: -7.634650, lng: 111.339693},
						{lat: -7.636437, lng: 111.337290}, {lat: -7.637883, lng: 111.336861},
						{lat: -7.637713, lng: 111.329909}, {lat: -7.632268, lng: 111.330080},
						{lat: -7.631673, lng: 111.328192}, {lat: -7.629631, lng: 111.328106},
						{lat: -7.629121, lng: 111.328621}, {lat: -7.627675, lng: 111.327505},
						{lat: -7.631077, lng: 111.326819}, {lat: -7.632268, lng: 111.328621},
						{lat: -7.635246, lng: 111.328621}, {lat: -7.633885, lng: 111.326475},
						{lat: -7.633459, lng: 111.325102}, {lat: -7.632354, lng: 111.325016},
						{lat: -7.632183, lng: 111.324330}, {lat: -7.630822, lng: 111.324158}, 
						{lat: -7.631248, lng: 111.322785}, {lat: -7.635926, lng: 111.323900},
						{lat: -7.639414, lng: 111.326733}, {lat: -7.642817, lng: 111.327591},
						{lat: -7.643923, lng: 111.327162}, {lat: -7.644944, lng: 111.323729},
						{lat: -7.639584, lng: 111.316433}, {lat: -7.639499, lng: 111.315317},
						{lat: -7.640010, lng: 111.315317}, {lat: -7.642562, lng: 111.317892},
						{lat: -7.644008, lng: 111.317978}, {lat: -7.644433, lng: 111.316605},
						{lat: -7.643838, lng: 111.315832}, {lat: -7.643668, lng: 111.314287},
						{lat: -7.645709, lng: 111.314888}, {lat: -7.646135, lng: 111.314116},
						{lat: -7.647496, lng: 111.314459}, {lat: -7.651239, lng: 111.320038},
						{lat: -7.653110, lng: 111.318665}, {lat: -7.652089, lng: 111.314802},
						{lat: -7.651919, lng: 111.313172}, {lat: -7.652770, lng: 111.311970},
						{lat: -7.652685, lng: 111.308708}, {lat: -7.654726, lng: 111.308279}, 
						{lat: -7.655917, lng: 111.308623}, {lat: -7.656428, lng: 111.308022},
						{lat: -7.658129, lng: 111.307850}, {lat: -7.658214, lng: 111.310082},
						{lat: -7.654811, lng: 111.312657}, {lat: -7.657108, lng: 111.317635},
						{lat: -7.658554, lng: 111.317377}, {lat: -7.658895, lng: 111.316347}, 
						{lat: -7.659915, lng: 111.315575}, {lat: -7.661447, lng: 111.311283},
						{lat: -7.665189, lng: 111.305962}, {lat: -7.666891, lng: 111.306134},
						{lat: -7.667911, lng: 111.307936}, {lat: -7.669783, lng: 111.308880},
						{lat: -7.669272, lng: 111.309996}, {lat: -7.666976, lng: 111.308966},
						{lat: -7.665615, lng: 111.309138}, {lat: -7.665019, lng: 111.310597},
						{lat: -7.665274, lng: 111.313257}, {lat: -7.664169, lng: 111.315575},
						{lat: -7.664934, lng: 111.316262}, {lat: -7.665785, lng: 111.316777},
						{lat: -7.665360, lng: 111.318579}, {lat: -7.664254, lng: 111.318665},
						{lat: -7.658129, lng: 111.323214}, {lat: -7.659575, lng: 111.323385},
						{lat: -7.660171, lng: 111.324072}, {lat: -7.661106, lng: 111.323986},
						{lat: -7.662637, lng: 111.325960}, {lat: -7.662467, lng: 111.327334},
						{lat: -7.662723, lng: 111.328964}, {lat: -7.661617, lng: 111.326905}, 
						{lat: -7.660681, lng: 111.326819}, {lat: -7.659915, lng: 111.327849},
						{lat: -7.661191, lng: 111.329308}, {lat: -7.661106, lng: 111.331711},
						{lat: -7.660256, lng: 111.332827}, {lat: -7.660681, lng: 111.337719},
						{lat: -7.662212, lng: 111.339350}, {lat: -7.661447, lng: 111.339951},
						{lat: -7.658299, lng: 111.337891}, {lat: -7.656087, lng: 111.339607},
						{lat: -7.655917, lng: 111.340809}, {lat: -7.656768, lng: 111.340809},
						{lat: -7.655747, lng: 111.350765}, {lat: -7.655917, lng: 111.353512},
						{lat: -7.655152, lng: 111.353770}, {lat: -7.654897, lng: 111.355057},
						{lat: -7.652004, lng: 111.355229}, {lat: -7.651579, lng: 111.356516},
						{lat: -7.649367, lng: 111.354199}, {lat: -7.649452, lng: 111.356774},
						{lat: -7.646475, lng: 111.356859}, {lat: -7.646050, lng: 111.354199},
						{lat: -7.644689, lng: 111.354027}, {lat: -7.644774, lng: 111.357460},
						{lat: -7.640775, lng: 111.357289}, {lat: -7.640265, lng: 111.356516}
					];

					//contruct the polygon
					var magetanPoly = new google.maps.Polygon({
						paths: magetanPol, strokeColor: '#FF0000',
						strokeOpacity: 0.8, strokeWeight: 3,
						fillColor: '#FF0000', fillOpacity: 0.35
					});
					magetanPoly.setMap(map);

					//add listener
					magetanPoly.addListener('click', showArrays);

					infoWindow = new google.maps.InfoWindow;
				}

				function showArrays(event){
					var vertices = this.getPath();
					var contentString = '<b>Bermuda Triangle polygon</b><br>' +
					'Clicked location: <br>' + event.latLng.lat() + ',' + event.latLng() +
					'<br>';

					//iterate
					for (var i = 0; i < vertices.getLength(); i++) {
						var xy = vertices.getAt(i);
						contentString += '<br>' + 'Coordinate' + i + ':<br>' + xy.lat() + ',' +
						xy.lng();
					};

					//replace the info window
					infoWindow.setContent(contentString);
					infoWindow.setPosition(event.latLng);

					infoWindow.open(map);
				}
				// ini batas tambahan baru
			</script>
			
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBQcOydZmjfvdGFyiVqpDHxJFatcgYBfs4&callback=initMap"
			async defer></script>

			<!-- End Map -->
			
			<!-- Tabel -->
			
	        <div class="w3-row w3-margin">
	            <div class="w3-col l1 w3-left">
	                &nbsp;
	            </div>

	            <div class="w3-col l1 w3-right">
	                &nbsp;
	            </div>

	            <div class="w3-rest">
	                <div class="w3-border-bottom w3-margin-bottom">
	                    <h3 class="w3-margin-left" style="font-weight: 300"> Data Permintaan</h3>
	                </div>
	                
					<?php if($this->session->flashdata('info') != NULL) { ?>
						<div class="w3-panel cursor-pointer w3-green" onclick="w3.hide(this)">
							<p><?= $this->session->flashdata('info'); ?></p>
						</div>
					<?php } else if($this->session->flashdata('error') != NULL) { ?>
						<div class="w3-panel cursor-pointer w3-red" onclick="w3.hide(this)">
							<p><?= $this->session->flashdata('error'); ?></p>
						</div>
					<?php } ?>
					
					<div id="view-daging" w3-include-html="<?= base_url(); ?>map/read_permintaan/daging" style="display:none"></div>
					<div id="view-telur" w3-include-html="<?= base_url(); ?>map/read_permintaan/telur" style="display:none"></div>
	            </div>
	        </div>
        </div>
        
        <script>
			var jenis = "daging";
			var idClicked;
			
            function viewClick(id, lat, lang) {
				resetViewClick();
				
				if(id != idClicked) {
					var idElement = '#' + jenis + '-view-btn-' + id;
					idClicked = id;
					
					//~ window.alert(lat + " " + lang);
					
					initMap(13, lat, lang);
					
					w3.removeClass(idElement, 'w3-theme-d1');
					w3.addClass(idElement, 'w3-theme-l1');
				} else {
					idClicked = '';
					
					initMap();
				}
            }
            
            function resetViewClick() {
				var idElement = '#' + jenis + '-view-btn-' + idClicked;
				
				w3.removeClass(idElement, 'w3-theme-l1');
				w3.addClass(idElement, 'w3-theme-d1');
			}
            
            function editClick(id = 1) {
                w3.hide(".permintaan-" + id);
                w3.show(".permintaan-edit-" + id);
            }
            
            function cancelClick(id = 1) {
                w3.hide(".permintaan-edit-" + id);
                w3.show(".permintaan-" + id);
            }
            
            function saveClick(jenis, id) {
				var permintaan = document.getElementById(jenis + "-input-" + id).value;
				window.open("<?= base_url(); ?>permintaan/update?jenis=" + jenis + "&id=" + id + "&permintaan=" + permintaan, "_self");
            }
            
            function showTabel(menu = "daging") {
				if(menu == "telur") {
					w3.hide("#view-daging");
					w3.show("#view-telur");
				} else {
					w3.hide("#view-telur");
					w3.show("#view-daging");
				}
				
				jenis = menu;
				
				w3.includeHTML();
			}
			
			showTabel();
        </script>
        
        <!-- End Tabel -->
		
		<!-- Footer -->

		<center>
			<p style="font-size: 15px;">2017 &copy; Pak Bejan Jual Ayam</p>
		</center>
		
		<!-- End Footer -->
	</body>

</html>

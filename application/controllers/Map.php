<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Map extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('session');
		$this->load->model('ayam_model');
	}
	
	public function index()
	{		
		$this->load->view('index');
	}
	
	public function update_permintaan()
	{
		$jenis = $this->input->get("jenis");
		$id = $this->input->get("id");
		$permintaan = $this->input->get("permintaan");
		$result = false;
		
		if($jenis == "daging") {
			$result = $this->ayam_model->update_permintaan_daging($id, $permintaan);
		} else if($jenis == "telur") {
			$result = $this->ayam_model->update_permintaan_telur($id, $permintaan);
		}
		
		if($result == true) {
			$this->session->set_flashdata('info', 'Berhasil menyimpan data!');
		} else {
			$this->session->set_flashdata('error', 'Gagal menyimpan data!');
		}
		
		$this->session->set_flashdata('jenis', $jenis);
		
		redirect(base_url());
	}
	
	public function read_permintaan($jenis)
	{
		$data = NULL;
		
		if($jenis == "daging") {
			$data["permintaan"] = $this->ayam_model->get_permintaan_daging();
		} else if($jenis == "telur") {
			$data["permintaan"] = $this->ayam_model->get_permintaan_telur();
		}
		
		$data["jenis"] = $jenis;
		
		//~ print_r($data);
		
		$this->load->view('permintaan', $data);
	}
}

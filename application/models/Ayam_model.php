<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ayam_model extends CI_Model {
        
        public function __construct()
        {
                parent::__construct();
                $this->load->database();
        }
        
        public function get_permintaan_telur()
        {
			$this->db->select('*');
			$this->db->join('kecamatan', 'kecamatan.id = telur.id_kecamatan');
			$result = $this->db->get('telur')->result();
			
			return $result;
		}
		
		public function get_permintaan_daging()
        {
			$this->db->select('*');
			$this->db->join('kecamatan', 'kecamatan.id = daging.id_kecamatan');
			$result = $this->db->get('daging')->result();
			
			return $result;
		}
		
		public function update_permintaan_telur($id, $permintaan)
        {
			$data = array(
					'permintaan' => $permintaan
			);

			$this->db->where('id', $id);
			
			return $this->db->update('telur', $data);
		}
		
		public function update_permintaan_daging($id, $permintaan)
        {
			$data = array(
					'permintaan' => $permintaan
			);

			$this->db->where('id', $id);
			
			return $this->db->update('daging', $data);
		}
        
}

?>

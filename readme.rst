########################
Beyam: Bejan Jualan Ayam
########################

Beyam: Bejan Jualan Ayam adalah aplikasi berbasis web yang menampilkan
peta persebaran permintaan ayam potong dan telur ayam di sekitar daerah
peternakan Pak Bejan.

Aplikasi ini mencakup edit data permintaan per kecamatan dan peta persebaran
permintaan berbasis Google Map.

Aplikasi ini digunakan untuk mata kuliah Aplikasi dan Teknologi Web
pada semester 5. Dibangun dengan menggunakan CodeIgniter 3.1.6.

***************
Informasi Rilis
***************

Repo ini mungkin hanya digunakan sampai akhir semester 5, sampai
programnya selesai. Versi terbaru (meskipun tidak stabil) bisa
didapatkan dengan meng-clone-nya.

***************************
Changelog dan Fitur Terbaru
***************************

Repo ini belum menyediakan changelog dan fitur terbaru.

****************
Kebutuhan Server
****************

Mirip dengan kebutuhan untuk CodeIgniter. PHP versi 5.6 atau lebih baru
direkomendasikan.

Keterangan tambahan dari Codeigniter:

"It should work on 5.3.7 as well, but we strongly advise you NOT to run
such old versions of PHP, because of potential security and performance
issues, as well as missing features."

*********
Instalasi
*********

Instalasi bisa langsung dilakukan dengan meng-clone repo ini ke direktori
web server Anda.

*******
Lisensi
*******

Lisensi ikut dengan Codeigniter, silahkan lihat `license agreement
<https://github.com/bcit-ci/CodeIgniter/blob/develop/user_guide_src/source/license.rst>`_.

*********
Developer
*********

Kelompok Beyam Aplikasi dan Teknologi Web:
1. Muslim Aswaja
2. Rizki Maulidah
3. Rosi Ayu Wulandari
4. Satya Ayu Nurlita Asri